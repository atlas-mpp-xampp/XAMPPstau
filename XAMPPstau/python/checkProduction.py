#! /usr/bin/env python
from ClusterSubmission.Utils import ReadListFromFile, ResolvePath, WriteList, id_generator, RecursiveLS
from ClusterSubmission.PandaBookKeeper import getArgumentParser, interesting_grid_jobs, get_broken_jobs, write_broken_log
from ClusterSubmission.RequestToGroupDisk import initiateReplication
from ClusterSubmission.ListDisk import RUCIO_ACCOUNT, RUCIO_RSE
from XAMPPstau.SubmitToGrid import getAnalyses, getLogicalDataSets
from XAMPPbase.CreateMergedNTUP_PILEUP import GetPRW_datasetID
import logging, os

logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)


def setupRetryParser():
    parser = getArgumentParser()
    parser.set_defaults(sync_db=True)
    #  parser.set_defaults(automatic_retry=True)
    parser.add_argument(
        "--in_list_dir",
        help="Directory where all input datasets are stored. This one is mandatory if the script shall resubmit broken jobs",
        default=ResolvePath("XAMPPstau/SampleLists/mc16_13TeV/lephad/"))
    parser.add_argument("--destRSE", help="Replicate the data to these sites", default=[RUCIO_RSE], nargs="+")
    parser.add_argument("--replicate_ds", help="Replicate output datasets", default=False, action="store_true")

    parser.add_argument("--resubmit_broken", help="Automatically resubmit the broken jobs", default=False, action="store_true")
    parser.add_argument('--stream',
                        help='What analysis stream should be assigned',
                        choices=[ana for ana in getAnalyses().iterkeys()],
                        required=True)
    parser.add_argument("--production", help="Name of the production", required=True)
    parser.add_argument('--productionRole', help='specify optionally a production role you have e.g. perf-muons', default='')
    parser.add_argument('--noSyst', help='No systematics will be submitted', action="store_true", default=False)
    return parser


if __name__ == '__main__':
    options = setupRetryParser().parse_args()
    options.job_name_pattern = ["XAMPP.%s.%s." % (options.stream, options.production)]
    options.select_scopes = ["user.%s" % (RUCIO_ACCOUNT) if len(options.productionRole) == 0 else "group.%s" % (options.productionRole)]
    grid_jobs = interesting_grid_jobs(options)
    broken = get_broken_jobs(grid_jobs)

    to_replicate = []

    for job in sorted(grid_jobs, key=lambda x: x.jobName):
        ### Reduce the submitted grid jobs
        ### All running/done jobs can be distributed to the sites
        if job in broken: continue
        job_name = job.jobName
        if job_name.endswith("/"): job_name = job_name[:-1]

        to_replicate += ["%s_XAMPP" % (job_name)]
        if job.taskStatus != "done":
            physical_sample = job_name[job_name.find(".", job_name.find(options.production)) + 1:]
        #  print "\"%s\","%(physical_sample)
    ### Replicate the output datasets
    if options.replicate_ds:
        for rse in options.destRSE:
            initiateReplication(ListOfDataSets=to_replicate, Rucio=options.rucio, RSE=rse)
    if len(broken) > 0: write_broken_log(options, broken)
    if not options.resubmit_broken or len(broken) == 0: exit(0)
    to_resubmit = []

    in_ds_dir = ResolvePath(options.in_list_dir)
    if not in_ds_dir:
        logging.error("Invalid in_put_datset_path")
        exit(1)
    logging.info("Going to retrieve the list of datasets from %s" % (in_ds_dir))
    all_in_ds = []
    for ds_list in RecursiveLS(in_ds_dir, [
            "txt",
    ]):
        all_in_ds += ReadListFromFile(ds_list)
    if len(all_in_ds) == 0:
        logging.error("No valid datasets have been found")
        exit(1)
    for job in broken:
        job_name = job.jobName
        if job_name.endswith("/"): job_name = job_name[:-1]
        physical_sample = job_name[job_name.find(".", job_name.find(options.production)) + 1:]
        if physical_sample in getLogicalDataSets().iterkeys():
            to_resubmit += [
                ds for ds in all_in_ds if ds.find("data") == -1 and GetPRW_datasetID(ds) in getLogicalDataSets()[physical_sample]
            ]
        else:
            to_resubmit += job.inDS.split(",")

    tmp_file = "/tmp/%s.txt" % (id_generator(40))
    WriteList(to_resubmit, tmp_file)
    submit_cmd = "python %s --stream %s --production %s --sampleList %s %s %s --DuplicateTask" % (
        ResolvePath("XAMPPstau/python/SubmitToGrid.py"),
        options.stream,
        options.production,
        tmp_file,
        "" if len(options.productionRole) == 0 else "--productionRole %s" % (options.productionRole),
        "" if not options.noSyst else "--noSyst",
    )

    logging.info("Going to resubmit the %d broken jobs which are very likely due to failure" % (len(broken)))
    os.system(submit_cmd)
    os.system("rm %s" % (tmp_file))
