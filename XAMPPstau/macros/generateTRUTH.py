#!/env/python
import argparse, commands, os
from XAMPPbase.CreateAODFromDAODList import convertToAOD
from XAMPPbase.CreateMergedNTUP_PILEUP import GetAMITagsMC, GetPRW_datasetID
from ClusterSubmission.Utils import CreateDirectory, ReadListFromFile, ResolvePath, WriteList, CheckRucioSetup, CheckRemainingProxyTime
from pprint import pprint

if __name__ == '__main__':
    CheckRucioSetup()
    CheckRemainingProxyTime()

    parser = argparse.ArgumentParser(
        description='This script converts DAOD filelists to AOD filelists which then can be used for creating pileup reweighting files.',
        prog='CreateAODFromDAODList',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--datasets', '-d', '-D', help='DAOD filelist to be converted into TRUTH', default='')
    parser.add_argument("--outFile", help="Write everything to", default="mc16_TRUTH3.txt")
    RunOptions = parser.parse_args()

    AODs = [convertToAOD(A) for A in ReadListFromFile(RunOptions.datasets)]
    EVNTS = []
    MISSING = []
    for A in AODs:
        Cmd = "rucio list-dids mc16_13TeV.%d*TRUTH3* --filter type=CONTAINER --short" % (GetPRW_datasetID(A))
        TRUTH1 = []  # [l.strip() for l in commands.getoutput(Cmd).split("\n") if len(l.strip()) > 0]
        Cmd1 = "rucio list-dids mc15_13TeV.%d*TRUTH3*p3401 --filter type=CONTAINER --short" % (GetPRW_datasetID(A))
        TRUTH1 += []  #[l.strip() for l in commands.getoutput(Cmd1).split("\n") if len(l.strip()) > 0]

        if len(TRUTH1) == 0:
            Cmd = "rucio list-dids mc16_13TeV.%d*EVNT* --filter type=CONTAINER --short" % (GetPRW_datasetID(A))
            MISSING += [l.strip() for l in commands.getoutput(Cmd).split("\n") if len(l.strip()) > 0]
        EVNTS += TRUTH1
    WriteList(sorted(EVNTS), RunOptions.outFile)
    WriteList(MISSING, "Missing.txt")
