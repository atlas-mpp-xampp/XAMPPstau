#ifndef XAPMPPstau_StauTriggerTool_H
#define XAPMPPstau_StauTriggerTool_H

#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <XAMPPbase/SUSYTriggerTool.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace TrigConf {
    class ITrigConfigTool;
}
namespace Trig {
    // Need the TrigDecisionTool directly for getChainGroup, features, and GetPreScale
    class TrigDecisionTool;
    class IMatchingTool;
    class FeatureContainer;
}  // namespace Trig

namespace XAMPP {
    class StauTriggerTool : public SUSYTriggerTool {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauTriggerTool, XAMPP::ITriggerTool)
        StauTriggerTool(const std::string& myname);

        virtual StatusCode initialize();
        virtual bool CheckTriggerMatching();
        virtual ~StauTriggerTool();

    private:
        ToolHandle<TrigConf::ITrigConfigTool> m_trigConfTool;
        DataVectorStorage* m_trigger_tau_store;
    };
}  // namespace XAMPP
#endif
