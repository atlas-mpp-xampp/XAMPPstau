#ifndef XAPMPPstau_StauTauSelector_H
#define XAPMPPstau_StauTauSelector_H
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <TRandom3.h>
#include <XAMPPbase/SUSYTauSelector.h>

#include <fstream>
#include <iostream>
#include <sstream>
namespace TauAnalysisTools {
    class ITauSelectionTool;
}
namespace XAMPP {
    class IJetSelector;
    class StauTauSelector : public SUSYTauSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauTauSelector, XAMPP::ITauSelector)
        StauTauSelector(const std::string& myname);

        StatusCode initialize() override;
        StatusCode InitialFill(const CP::SystematicSet& systset) override;
        StatusCode FillTaus(const CP::SystematicSet& systset) override;
        virtual ~StauTauSelector();

    private:
        StatusCode TauPromotion(const CP::SystematicSet& systset);
        ToolHandle<XAMPP::IJetSelector> m_jet_selection;
        // How many tau-leptons shall be propagated to the OR
        int m_NumTauCandidate_OR;
        // Options to perform tau promotion of the Had-had channel
        bool m_performTauPromotion;
        bool m_performFakeEff;
        IntVector m_promotion_dsids;
        unsigned int m_random_seed;
        bool m_promotion_before_OR;
        std::unique_ptr<TRandom3> m_rnd_generator;
        // Tau promotion needs some extra variables store in the ntuples
        XAMPP::Storage<int>* m_NumPoolPromotedTaus;
        XAMPP::Storage<int>* m_NumFakeTaus;
        XAMPP::Storage<int>* m_NumAllFakeTaus;
        XAMPP::Storage<char>* m_FlagPromotedEvent;
    };
}  // namespace XAMPP
#endif
