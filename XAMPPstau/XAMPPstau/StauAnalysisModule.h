#ifndef StauAnalysisModule_H
#define StauAnalysisModule_H
#include <XAMPPbase/BaseAnalysisModule.h>

namespace XAMPP {
    class StauAnalysisModule : public BaseAnalysisModule, virtual public IAnalysisModule {
    public:
        StauAnalysisModule(const std::string& myname);
        virtual ~StauAnalysisModule();

    protected:
        StatusCode retrieveContainers();
        // Pointer to the particle storage objects which are need to calculate the module jobs
        XAMPP::ParticleStorage* m_electrons;
        XAMPP::ParticleStorage* m_muons;
        XAMPP::ParticleStorage* m_taus;
        XAMPP::ParticleStorage* m_jets;
        XAMPP::Storage<XAMPPmet>* m_met;

    private:
        std::string m_elec_container;
        std::string m_muon_container;
        std::string m_tau_container;
        std::string m_jet_container;
        std::string m_met_container;
    };

}  // namespace XAMPP

#endif
