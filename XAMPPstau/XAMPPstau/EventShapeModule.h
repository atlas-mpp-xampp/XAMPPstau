#ifndef EventshapeModule_H
#define EventshapeModule_H

#include <XAMPPbase/SUSYAnalysisHelper.h>
#include <XAMPPstau/StauAnalysisModule.h>

class EventShapeVariablesCalc;

namespace XAMPP {
    class EventShapeModule : public StauAnalysisModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(EventShapeModule, XAMPP::IAnalysisModule)
        //
        EventShapeModule(std::string myname);
        virtual StatusCode bookVariables();
        virtual StatusCode fill();

        virtual ~EventShapeModule();

    private:
        // Event shape options
        std::unique_ptr<EventShapeVariablesCalc> m_EventShapeCalc;

        bool m_Evshape_Sphericity;
        bool m_Evshape_Spherocity;
        bool m_Evshape_Thrust;

        bool m_Evshape_FoxWolfram;
        bool m_norm_FoxWolfram;
        unsigned int m_maxWolframMoments;

        std::vector<XAMPP::Storage<float>*> m_WolframMoments;
        XAMPP::Storage<bool>* m_valid_wolfram;

        XAMPP::Storage<float>* m_Sphericity;
        XAMPP::Storage<float>* m_Planarity;
        XAMPP::Storage<float>* m_Aplanarity;
        XAMPP::Storage<bool>* m_valid_sphericity;

        XAMPP::Storage<float>* m_Lin_Sphericity;
        XAMPP::Storage<float>* m_Lin_Sphericity_C;
        XAMPP::Storage<float>* m_Lin_Sphericity_D;
        XAMPP::Storage<float>* m_Lin_Planarity;
        XAMPP::Storage<float>* m_Lin_Aplanarity;
        XAMPP::Storage<bool>* m_valid_Lin_sphericity;

        XAMPP::Storage<float>* m_Thrust;
        XAMPP::Storage<float>* m_ThrustMajor;
        XAMPP::Storage<float>* m_ThrustMinor;
        XAMPP::Storage<float>* m_Oblateness;
        XAMPP::Storage<bool>* m_valid_thrust;

        XAMPP::Storage<float>* m_Spherocity;
        XAMPP::Storage<bool>* m_valid_Spherocity;

        XAMPP::Storage<float>* m_circularity;
        XAMPP::Storage<float>* m_Lin_circularity;
    };
}  // namespace XAMPP
#endif
