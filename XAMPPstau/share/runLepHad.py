from XAMPPstau.SubmitToGrid import getLogicalDataSets
from ClusterSubmission.Utils import ClearFromDuplicates
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Someimes? Alsmost the entire day
include("XAMPPstau/StauToolSetup.py")
AssembleIO()

from AthenaCommon.Constants import WARNING, DEBUG, VERBOSE

ServiceMgr.MessageSvc.debugLimit = 200000

SetupSystematicsTool(noJets=False, noBtag=False, noElectrons=False, noMuons=False, noTaus=False, noDiTaus=True, noPhotons=True)
SetupStauAnaHelper(InitLepHadChannel=True)
### Activate the LHE weights
SetupStauAnaHelper().createCommonTree = True
SetupSystematicsTool().pruneSystematics = [
    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up",
    "MUON_EFF_BADMUON_STAT__1down",
    "MUON_EFF_BADMUON_STAT__1up",
    "MUON_EFF_BADMUON_SYS__1down",
    "MUON_EFF_BADMUON_SYS__1up",
    "MUON_EFF_RECO_STAT_LOWPT__1down",
    "MUON_EFF_RECO_STAT_LOWPT__1up",
    "MUON_EFF_RECO_SYS_LOWPT__1down",
    "MUON_EFF_RECO_SYS_LOWPT__1up",
]

### Get rid of the stupdily large event weights in the case we're running over sherpa
for sample, DSIDS in getLogicalDataSets().iteritems():
    if sample.find("Sherpa") != -1 and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        print "We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 100." % (
            getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber())
        ### remove the crazy events from the meta data
        SetupStauAnaHelper().OutlierWeightStrategy = 1
        break

#####################################################################
#       Ensure that the matching branches are written to the tree   #
#####################################################################
SetupSUSYTriggerTool().StoreMatchingInfo = False
#use baseline objects for matching
SetupSUSYTriggerTool().SignalElecForMatching = False
SetupSUSYTriggerTool().SignalMuonForMatching = False
SetupSUSYTriggerTool().SignalTauForMatching = False

# the lephad std config file
STFile = "XAMPPstau/SUSYTools_Stau_LepHad.conf"
ParseCommonStauOptions(STFile=STFile,
                       LepHadStream=True,
                       doEventShapes=True,
                       writeBaselineSF=False,
                       doMetSignificance=True,
                       doTauPromotion=False,
                       includeTausInMet=True)

SetupStauAnaHelper().fillLHEWeights = True
### Include the TrackMet for QCD rejection
SetupSUSYMetSelector().DoTrackMet = True
SetupStauAnaHelper().DoTrackMet = True

try:
    SetupStauAnaHelper().DoTauBJetOR = False
    SetupStauAnaHelper().DoSampleMerging = True
except:
    logging.warning("OR.TauBjet not found in the config file. Please check")

### Disable the isolation signal requirement for electrons and muons -> they're counted as signal
SetupSUSYElectronSelector().RequireIsoSignal = False
SetupSUSYMuonSelector().RequireIsoSignal = False
SetupStauTauSelector().SignalDecorator = "passRNNID"

### Add another module for the raw wolfram moments
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon import CfgMgr
from XAMPPstau.XAMPPstauConf import XAMPP__EventShapeModule

Module = CfgMgr.XAMPP__EventShapeModule("RawWolframMoments")
Module.CalculateSphericity = False
Module.CalculateSpherocity = False
Module.CalculateThrust = False
Module.CalculateFoxWolfram = True
Module.NormalizeWolframMoments = False
Module.OrderWolframMoments = 12

Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "candidate_taus"
Module.JetContainer = "signal_jets"
Module.MissingEt = "MetTST"
Module.GroupName = "UnNormed"
ToolSvc += Module
SetupStauAnaHelper().AnalysisModules += [Module]

from XAMPPstau.XAMPPstauConf import XAMPP__StauMT2Module

Module = CfgMgr.XAMPP__StauMT2Module("LepLepMT2")
Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "signal_lightlep"
Module.JetContainer = "signal_jets"
Module.MissingEt = "MetTST"
Module.GroupName = "ElMu"
ToolSvc += Module
SetupStauAnaHelper().AnalysisModules += [Module]

#from XAMPPstau.XAMPPstauConf import XAMPP__StauJigSawSusyModule
#JigSawModuleStauStau = CfgMgr.XAMPP__StauJigSawSusyModule("SUSYJigSaw")
#JigSawModuleStauStau.ElectronContainer = "signal_electrons"
#JigSawModuleStauStau.MuonContainer = "signal_muons"
#JigSawModuleStauStau.TauContainer = "candidate_taus"
#JigSawModuleStauStau.JetContainer = "signal_jets"
#JigSawModuleStauStau.MissingEt = "MetTST"
#JigSawModuleStauStau.GroupName = "RJStauStau"
#ToolSvc += JigSawModuleStauStau
#SetupStauAnaHelper().AnalysisModules += [JigSawModuleStauStau]

#electrons
SetupSUSYElectronSelector().ApplyTriggerSF = True
SetupSUSYMuonSelector().ApplyTriggerSF = True
SetupSUSYElectronSelector().SeparateSF = True
SetupSUSYMuonSelector().SeparateSF = True
#trigger menu for electrons
SingleElectronTriggers2015 = [
    "HLT_e24_lhmedium_L1EM20VH", "HLT_e60_lhmedium", "HLT_e120_lhloose", "HLT_e17_lhmedium_nod0", "HLT_e17_lhmedium_nod0_ivarloose"
]

SingleElectronTriggers2016_2017 = ["HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"]

#Electron Trig SF
EleTrigSFConfig = [
    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",
    "E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI",
]

#Electron Trig Matching for SF
ElectronTriggerSF = [
    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",
    "e17_lhmedium_nod0_L1EM15HI_OR_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI"
]

#Muon triggers
SingleMuonTriggers2015 = ["HLT_mu20_iloose_L1MU15", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2016 = ["HLT_mu24_ivarmedium", "HLT_mu26_ivarmedium", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2017 = ["HLT_mu26_ivarmedium", "HLT_mu50"]
SingleMuonTriggers2018 = ["HLT_mu26_ivarmedium", "HLT_mu50"]
SingleTauTriggers = [
    "HLT_tau80_medium1_tracktwo_L1TAU60",
    "HLT_tau25_medium1_tracktwo",
    #     "HLT_tau25_medium1_tracktwoEF"   ,
    #    "HLT_tau25_mediumRNN_tracktwoMVA",
    "HLT_tau35_medium1_tracktwo",
    #  "HLT_tau35_medium1_tracktwoEF",
    #  "HLT_tau35_mediumRNN_tracktwoMVA",
]
# muon trig SF
MuonTriggerSF2015 = [
    "HLT_mu20_iloose_L1MU15_OR_HLT_mu40",
    "HLT_mu14",
]

MuonTriggerSF2016 = [
    "HLT_mu24_ivarmedium_OR_HLT_mu40",
    "HLT_mu24_ivarmedium_OR_HLT_mu50",
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
    "HLT_mu14_ivarloose",
]

MuonTriggerSF2017 = [
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
    "HLT_mu14_ivarloose",
]
MuonTriggerSF2018 = [
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
    "HLT_mu14_ivarloose",
]
# Tau trig SF
TauTriggerSF = [
    "HLT_tau25_medium1_tracktwo",
    "HLT_tau35_medium1_tracktwo",
    "HLT_tau80L1TAU60_medium1_tracktwo",
]
#tau-lepton trig
CombinedTauTriggers = [
    #mu-tau
    "HLT_mu14_tau35_medium1_tracktwo",
    "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
    #"HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
    #"HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
    #e-tau
    "HLT_e17_lhmedium_nod0_tau80_medium1_tracktwo",
    "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",

    #New triggers for Run3: https://its.cern.ch/jira/browse/ATR-19984
    #"HLT_mu20_ivarloose_tau20_mediumRNN_tracktwoMVA_L1TAU8_03dRtt",
    #"HLT_e24_lhmedium_nod0_ivarloose_L1EM22VHI_tau20_mediumRNN_tracktwoMVA_L1TAU8_03dRtt"
]

#Lepton Triggers
SingleLepTriggers = sorted(
    ClearFromDuplicates(SingleMuonTriggers2015 + SingleMuonTriggers2016 + SingleMuonTriggers2017 + SingleMuonTriggers2018 +
                        CombinedTauTriggers + SingleElectronTriggers2015 + SingleElectronTriggers2016_2017 + SingleTauTriggers))

SetupSUSYTriggerTool().Triggers = SingleLepTriggers
SetupSUSYTriggerTool().DisableSkimming = False

SetupSUSYElectronSelector().SFTrigger = ElectronTriggerSF
SetupSUSYElectronSelector().TriggerSFconfig = EleTrigSFConfig

### Thus far the Tau egamma trigger scale-factors are not in the map. Copy the old map file and
### add them
from ClusterSubmission.Utils import WriteList, ReadListFromFile, ResolvePath

trig_sf_map = WriteList(
    ReadListFromFile("ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt") + [
        "Eff_E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Tight_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.TightLLH_d0z0_v13_isolFCTight.root",
        "E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Tight_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.TightLLH_d0z0_v13_isolFCTight.root",
        "Eff_E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Medium_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.MediumLLH_d0z0_v13_isolFCTight.root",
        "E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Medium_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.MediumLLH_d0z0_v13_isolFCTight.root",
    ], "%s/EGammaSF.txt" % (os.getcwd()))
SetupSUSYElectronSelector().EfficiencyMapFilePath = trig_sf_map
SetupSUSYElectronSelector().RequireTrigMatchingSF = False
SetupSUSYMuonSelector().SFTrigger2015 = MuonTriggerSF2015
SetupSUSYMuonSelector().SFTrigger2016 = MuonTriggerSF2016
SetupSUSYMuonSelector().SFTrigger2017 = MuonTriggerSF2017
SetupSUSYMuonSelector().SFTrigger2018 = MuonTriggerSF2018

SetupSUSYTauSelector().TrigMatchingForTrigSF = True
SetupSUSYTauSelector().SFTrigger = TauTriggerSF
#SetupSUSYTauSelector().BaselinePtCut = 40000.
#

ElectronWPs = [
    #    "FCHighPtCaloOnly",
    #    "Gradient",
    #    "FCLoose",
    #    "FCTight",
    #    "FixedCutPflowTight",
    #    "FixedCutPflowLoose",
    #    "FCLoose_FixedRad",
    #    "FCTight_FixedRad",
    #    "FCTightTrackOnly_FixedRad",
]
MuonWPs = [
    # "FixedCutHighPtTrackOnly",
    # "FCTight",
    # "FCLoose",
    # "FCTightTrackOnly",
    # "FCTight_FixedRad",
    # "FCLoose_FixedRad",
    # "FCTightTrackOnly_FixedRad",
    #    "FixedCutPflowLoose",
    #    "FixedCutPflowTight",
    # "FixedCutPflowLoose",
    # "FixedCutPflowTight",
]
for E in ElectronWPs:
    SetupStauAnaHelper().IsolationWPTools += [AddIsolationSelectionTool(ElectronWP=E)]
for M in MuonWPs:
    SetupStauAnaHelper().IsolationWPTools += [AddIsolationSelectionTool(MuonWP=M)]

SetupAlgorithm()
