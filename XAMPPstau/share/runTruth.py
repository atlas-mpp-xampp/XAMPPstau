import os
from XAMPPstau.SubmitToGrid import getLogicalDataSets
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Someimes? Alsmost the entire day
include("XAMPPstau/StauToolSetup.py")
AssembleIO()

SetupTruthStauAnaHelper().OutputLevel = 0

SetupTruthStauAnaHelper().fillLHEWeights = True
SetupTruthStauAnaHelper().DoSampleMerging = True
### Get rid of the stupdily large event weights in the case we're running over sherpa
for sample, DSIDS in getLogicalDataSets().iteritems():
    if sample.find("Sherpa") != -1 and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        print "We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 100." % (
            getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber())
        ### remove the crazy events from the meta data
        SetupTruthStauAnaHelper().OutlierWeightStrategy = 1
        break
SetupTruthStauAnaHelper().TauCandidatesForOR = 2
SetupStauTruthAnalysisConfig().ActiveCutflows = [
    "ElectronEvent",
    "MuonEvent",
    "2#tau",
]
SetupTruthSelector().ElectronContainer = "TruthElectrons"
SetupTruthSelector().MuonContainer = "TruthMuons"
SetupTruthSelector().TauContainer = "TruthTaus"
SetupTruthSelector().NeutrinoContainer = "TruthNeutrinos"
SetupTruthSelector().JetContainer = "AntiKt4TruthDressedWZJets"
SetupTruthSelector().JetContainer = "AntiKt4TruthJets"

SetupTruthSelector().doTruthParticles = False  #not getFlags().isTRUTH3()
SetupTruthSelector().isTRUTH3 = True  #getFlags().isTRUTH3()
SetupTruthSelector().doJets = True
SetupTruthSelector().rejectUnknownOrigin = True

SetupTruthSelector().BaselineDecorator = "passOR"
SetupTruthSelector().SignalHardProcess = True

SetupTruthSelector().TruthBaselineMuonPtCut = 5000
SetupTruthSelector().TruthBaselineMuonEtaCut = 2.7
### Account for trigger acceptance in the MS
SetupTruthSelector().TruthSignalMuonPtCut = 5000
SetupTruthSelector().TruthSignalMuonEtaCut = 2.5

SetupTruthSelector().TruthBaselineElectronPtCut = 5000
SetupTruthSelector().TruthBaselineElectronEtaCut = 2.47
SetupTruthSelector().TruthSignalElectronPtCut = 5000

SetupTruthSelector().TruthBaselineTauPtCut = 15000
SetupTruthSelector().TruthBaselineTauEtaCut = 2.7
SetupTruthSelector().OnlySelectHadronicTau = False
SetupTruthSelector().OnlySelectHadronicSignalTau = True

##SetupTruthSelector().ExcludeBaselineTauInEta = ["-1.52;-1.37", "1.37;1.52"]

SetupTruthSelector().OnlySelectHadronicBaseTau = False
SetupTruthSelector().TruthSignalTauPtCut = 20000
SetupTruthSelector().TruthSignalTauEtaCut = 2.47
SetupTruthSelector().ExcludeSignalTauInEta = ["-1.52;-1.37", "1.37;1.52"]

SetupTruthSelector().TruthBaselineJetPtCut = 20000
SetupTruthSelector().TruthBaselineJetEtaCut = 4.5
SetupTruthSelector().TruthSignalJetEtaCut = 2.8
SetupTruthSelector().TruthBJetEtaCut = 2.5

from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon import CfgMgr

from XAMPPstau.XAMPPstauConf import XAMPP__StauMT2Module

MT2Module = CfgMgr.XAMPP__StauMT2Module("MT2Variables")
MT2Module.ElectronContainer = "signal_electrons"
MT2Module.MuonContainer = "signal_muons"
MT2Module.JetContainer = "signal_jets"
MT2Module.TauContainer = "signal_leptons"
MT2Module.MissingEt = "TruthMET"
ToolSvc += MT2Module
SetupTruthStauAnaHelper().AnalysisModules += [MT2Module]

MT2Module = CfgMgr.XAMPP__StauMT2Module("MT2Variables_fake")
MT2Module.ElectronContainer = "signal_electrons"
MT2Module.MuonContainer = "signal_muons"
MT2Module.JetContainer = "signal_jets"
MT2Module.TauContainer = "fake_leptons"
MT2Module.GroupName = "Fake"
MT2Module.MissingEt = "TruthMET"
ToolSvc += MT2Module
SetupTruthStauAnaHelper().AnalysisModules += [MT2Module]

################################################
#                Meff module                   #
################################################
from XAMPPstau.XAMPPstauConf import XAMPP__StauMeffModule

MeffModule = CfgMgr.XAMPP__StauMeffModule("MeffVariables")
MeffModule.ElectronContainer = "signal_electrons"
MeffModule.MuonContainer = "signal_muons"
MeffModule.TauContainer = "signal_taus"
##  The fake leptons is actually not the best name since this container includes the leading jet if
##  there is no tau in the event in order to fake the contribution of fake-taus on truth-level
MeffModule.LeptonContainer = "fake_leptons"
MeffModule.JetContainer = "signal_jets"
MeffModule.MissingEt = "TruthMET"
ToolSvc += MeffModule
SetupTruthStauAnaHelper().AnalysisModules += [MeffModule]

from XAMPPstau.XAMPPstauConf import XAMPP__EventShapeModule

Module = CfgMgr.XAMPP__EventShapeModule("EventShapeVariables")
Module.CalculateSphericity = True
Module.CalculateSpherocity = True
Module.CalculateThrust = True
Module.CalculateFoxWolfram = True
Module.NormalizeWolframMoments = True
Module.OrderWolframMoments = 12

Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "signal_taus"
Module.JetContainer = "signal_jets"
Module.MissingEt = "TruthMET"

ToolSvc += Module
SetupTruthStauAnaHelper().AnalysisModules += [Module]

Module = CfgMgr.XAMPP__EventShapeModule("RawWolframMoments")
Module.CalculateSphericity = False
Module.CalculateSpherocity = False
Module.CalculateThrust = False
Module.CalculateFoxWolfram = True
Module.NormalizeWolframMoments = False
Module.OrderWolframMoments = 12

Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "signal_taus"
Module.JetContainer = "signal_jets"
Module.MissingEt = "TruthMET"
Module.GroupName = "UnNormed"
ToolSvc += Module
SetupTruthStauAnaHelper().AnalysisModules += [Module]

from XAMPPstau.XAMPPstauConf import XAMPP__StauJigSawZModule

Module = CfgMgr.XAMPP__StauJigSawZModule("ZJigSaw")
Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "fake_taus"
Module.JetContainer = "signal_jets"
Module.MissingEt = "TruthMET"
Module.GroupName = "RJZ"
#Module.UseInvisibleMassJigSaw = False
Module.UseMinDeltaTauMass = True
Module.storeRJFlavour = True

ToolSvc += Module
SetupTruthStauAnaHelper().AnalysisModules += [Module]

from XAMPPstau.XAMPPstauConf import XAMPP__StauJigSawWModule

Module = CfgMgr.XAMPP__StauJigSawWModule("WJigSaw")
Module.ElectronContainer = "signal_electrons"
Module.MuonContainer = "signal_muons"
Module.TauContainer = "fake_taus"
Module.JetContainer = "signal_jets"
Module.MissingEt = "TruthMET"
Module.storeRJFlavour = True
Module.GroupName = "RJW"

ToolSvc += Module
SetupTruthStauAnaHelper().AnalysisModules += [Module]

SetupAlgorithm()
