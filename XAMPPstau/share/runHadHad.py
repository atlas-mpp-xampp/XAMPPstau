from XAMPPstau.SubmitToGrid import getLogicalDataSets
from ClusterSubmission.Utils import ClearFromDuplicates
import os
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPstau/StauToolSetup.py")
AssembleIO(1)
SetupStauAnaHelper(InitLepHadChannel=False)

#This list is just for Testing of the power to handle tau triggers
SingleTauTriggers = [
    #	"HLT_tau25_idperf_track",
    #	"HLT_tau25_idperf_tracktwo",
    #	"HLT_tau25_perf_track",
    #	"HLT_tau25_perf_tracktwo",
    #	"HLT_tau25_perf_tracktwo_L1TAU12",
    #	"HLT_tau25_loose1_tracktwo",
    #	"HLT_tau25_tight1_tracktwo",
    #	"HLT_tau25_medium1_tracktwo_L1TAU12",
    #	"HLT_tau160_perf_tracktwo",
    #	"HLT_tau160_idperf_track",
    #	"HLT_tau160_idperf_tracktwo",
    "HLT_tau25_medium1_tracktwo",
    "HLT_tau35_medium1_tracktwo",
    "HLT_tau50_medium1_tracktwo_L1TAU12",
    "HLT_tau60_medium1_tracktwo",
    "HLT_tau60_medium1_tracktwoEF",
    "HLT_tau80_medium1_tracktwo",
    "HLT_tau80_medium1_tracktwo_L1TAU60",
    "HLT_tau80_medium1_tracktwoEF_L1TAU60",
    #"HLT_tau35_medium1_tracktwoEF",
    #"HLT_tau40_medium1_tracktwo",
    #"HLT_tau25_medium1_tracktwoEF",
    #"HLT_tau25_mediumRNN_tracktwoMVA",
    #"HLT_tau35_mediumRNN_tracktwoMVA",
    #"HLT_tau60_mediumRNN_tracktwoMVA",
    #"HLT_tau80_mediumRNN_tracktwoMVA",
    #"HLT_tau40_medium1_tracktwoEF",
    #"HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60",
    #"HLT_tau40_mediumRNN_tracktwoMVA",
]
CombinedTauTriggers = [
    ### Ditau + MET
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
    ### Asmy ditau
    "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
    ### Ditau (or + 1J). Use + 1J in normal study. No 1J could use in CR
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
    "HLT_tau40_medium1_tracktwo_tau35_medium1_tracktwo",
    "HLT_tau40_medium1_tracktwoEF_tau35_medium1_tracktwoEF",
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
    ### Others
    #"HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12",
    #"HLT_tau40_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA",
    #"HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
    #"HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50"
]
LeptonTauTriggers = [
    #"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
    #"HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo",
    #"HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",
    #"HLT_mu14_ivarloose_tau35_medium1_tracktwo",
    #"HLT_mu14_ivarloose_tau25_medium1_tracktwo",
    #"HLT_mu14_tau25_medium1_tracktwo_xe50",
    ###       not availabe until mc16e and data18
    #"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
    #"HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
    #"HLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
    #"HLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
    #"HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
    #"HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
    #"HLT_mu14_tau25_medium1_tracktwoEF_xe50",
    #"HLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50"
]
METTriggers = [
    ### Supported
    #"HLT_xe90_pufit_L1XE50", "HLT_xe100_pufit_L1XE50", "HLT_xe100_pufit_L1XE55", "HLT_xe110_pufit_L1XE50", "HLT_xe110_pufit_L1XE55",
    #"HLT_xe110_pufit_xe65_L1XE50", "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe120_pufit_L1XE50", "HLT_xe90_mht_L1XE50", "HLT_xe110_mht_L1XE50",
    #"HLT_xe70_mht"
    ### What we only use
    #"HLT_xe90_pufit_L1XE50", "HLT_xe110_mht_L1XE50", "HLT_xe110_pufit_L1XE55", "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe70_mht"
]
#trigger menu for electrons
SingleElectronTriggers2015 = ["HLT_e24_lhmedium_L1EM20VH", "HLT_e60_lhmedium", "HLT_e120_lhloose"]

SingleElectronTriggers2016_2018 = ["HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"]
#Muon triggers
SingleMuonTriggers2015 = ["HLT_mu20_iloose_L1MU15", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2016 = ["HLT_mu24_ivarmedium", "HLT_mu26_ivarmedium", "HLT_mu40", "HLT_mu50"]
SingleMuonTriggers2017_2018 = ["HLT_mu26_ivarmedium", "HLT_mu50"]

TauTriggers = sorted(
    ClearFromDuplicates(CombinedTauTriggers + SingleTauTriggers + LeptonTauTriggers + METTriggers + SingleElectronTriggers2015 +
                        SingleElectronTriggers2016_2018 + SingleMuonTriggers2015 + SingleMuonTriggers2016 + SingleMuonTriggers2017_2018))

SetupSUSYTriggerTool().Triggers = TauTriggers
SetupSUSYTriggerTool().DisableSkimming = False
#SetupSUSYTriggerTool().OutputLevel = DEBUG
#ServiceMgr.MessageSvc.debugLimit = 2000000

STFile = "XAMPPstau/SUSYTools_Stau.conf"

SetupStauAnaHelper().fillLHEWeights = True
SetupStauAnaHelper().StoreLHEByName = True
#SetupStauAnaHelper().DoTauBJetOR = True

#SetupStauAnaHelper().createCommonTree = True
SetupSystematicsTool().pruneSystematics = [
    #    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    #    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up",
    "MUON_EFF_BADMUON_STAT__1down",
    "MUON_EFF_BADMUON_STAT__1up",
    "MUON_EFF_BADMUON_SYS__1down",
    "MUON_EFF_BADMUON_SYS__1up",
    #    "MUON_EFF_RECO_STAT_LOWPT__1down",
    #    "MUON_EFF_RECO_STAT_LOWPT__1up",
    #    "MUON_EFF_RECO_SYS_LOWPT__1down",
    #    "MUON_EFF_RECO_SYS_LOWPT__1up",
    #    "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    #    "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
    #    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    #    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
]

### Get rid of the stupdily large event weights in the case we're running over sherpa
for sample, DSIDS in getLogicalDataSets().iteritems():
    if sample.find("Sherpa") != -1 and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        print "We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 100." % (
            getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber())
        ### remove the crazy events from the meta data
        SetupStauAnaHelper().OutlierWeightStrategy = 1
        break

#####################################################################
#       Ensure that the matching branches are written to the tree   #
#####################################################################

#Electron Trig SF
EleTrigSFConfig = [
    "SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
]

#Electron Trig Matching for SF
ElectronTriggerSF = [
    "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0"
]
# muon trig SF
MuonTriggerSF2015 = [
    "HLT_mu20_iloose_L1MU15_OR_HLT_mu40",
]

MuonTriggerSF2016 = [
    "HLT_mu24_ivarmedium_OR_HLT_mu40",
    "HLT_mu24_ivarmedium_OR_HLT_mu50",
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]

MuonTriggerSF2017 = [
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]
MuonTriggerSF2018 = [
    "HLT_mu26_ivarmedium_OR_HLT_mu50",
]
# Tau trig SF
TauTriggerSF = [
    "HLT_tau25_medium1_tracktwo",
    "HLT_tau35_medium1_tracktwo",
    "HLT_tau50L1TAU12_medium1_tracktwo",
    "HLT_tau60_medium1_tracktwo",
    "HLT_tau80L1TAU60_medium1_tracktwo",
    #"HLT_tau160_medium1_tracktwo",
    #"HLT_tau160L1TAU100_medium1_tracktwo",
    "HLT_tau60_medium1_tracktwoEF",
    "HLT_tau80L1TAU60_medium1_tracktwoEF",
    #"HLT_tau160L1TAU100_medium1_tracktwoEF",
]

SetupSUSYTriggerTool().StoreMatchingInfo = True
#use baseline objects for matching
SetupSUSYTriggerTool().SignalElecForMatching = False
SetupSUSYTriggerTool().SignalMuonForMatching = False
SetupSUSYTriggerTool().SignalTauForMatching = False

#taus
SetupSUSYTauSelector().TrigMatchingForTrigSF = True
SetupSUSYTauSelector().SFTrigger = TauTriggerSF

#electrons
SetupSUSYElectronSelector().ApplyTriggerSF = True
SetupSUSYMuonSelector().ApplyTriggerSF = True
SetupSUSYElectronSelector().SeparateSF = True
SetupSUSYMuonSelector().SeparateSF = True
#Lepton Triggers
SetupSUSYElectronSelector().SFTrigger = ElectronTriggerSF
SetupSUSYElectronSelector().TriggerSFconfig = EleTrigSFConfig

### Thus far the Tau egamma trigger scale-factors are not in the map. Copy the old map file and
### add them
from ClusterSubmission.Utils import WriteList, ReadListFromFile, ResolvePath

trig_sf_map = WriteList(
    ReadListFromFile("ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt") + [
        "Eff_E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Tight_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.TightLLH_d0z0_v13_isolFCTight.root",
        "E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Tight_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.TightLLH_d0z0_v13_isolFCTight.root",
        "Eff_E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Medium_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.MediumLLH_d0z0_v13_isolFCTight.root",
        "E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_OR_e17_lhvloose_nod0_L1EM15VHI_Medium_FCTight=ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.E_TAU_2015_2016_e17_lhmedium_nod0_L1EM15HI_2017_2018_e17_lhmedium_nod0_AND_e17_lhvloose_nod0_L1EM15VHI.MediumLLH_d0z0_v13_isolFCTight.root",
    ], "%s/EGammaSF.txt" % (os.getcwd()))
SetupSUSYElectronSelector().EfficiencyMapFilePath = trig_sf_map
SetupSUSYElectronSelector().RequireTrigMatchingSF = False
SetupSUSYMuonSelector().SFTrigger2015 = MuonTriggerSF2015
SetupSUSYMuonSelector().SFTrigger2016 = MuonTriggerSF2016
SetupSUSYMuonSelector().SFTrigger2017 = MuonTriggerSF2017
SetupSUSYMuonSelector().SFTrigger2018 = MuonTriggerSF2018

ParseCommonStauOptions(STFile,
                       LepHadStream=False,
                       doQCDTree=False,
                       doEventShapes=False,
                       writeBaselineSF=False,
                       doMetSignificance=True,
                       includeTausInMet=True,
                       doTauPromotion=False,
                       useMC16d=True,
                       use17Data=True,
                       use18Data=True)
### List of DSIDs where Tau promotion shall be applied only
SetupStauTauSelector().doPromotionOnlyFor = []
SetupStauTauSelector().doFakeEff = False
SetupAlgorithm()
