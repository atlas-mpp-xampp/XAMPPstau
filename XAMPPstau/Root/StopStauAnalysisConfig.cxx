#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StopStauAnalysisConfig.h>

namespace XAMPP {
    StopStauAnalysisConfig::StopStauAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {}

    StatusCode StopStauAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        //          Stop-Stau stream
        //########################################################################
        CutFlow StopStau("tautau");

        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=1", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;
        StopStau.push_back(Least1BaselineTauCut);

        Cut* Least1BJet = NewSkimmingCut("N_{bjet}>=1", Cut::CutType::CutInt);
        if (!Least1BJet->initialize("n_BJets", ">=1")) return StatusCode::FAILURE;
        StopStau.push_back(Least1BJet);

        ATH_CHECK(AddToCutFlows(StopStau));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
