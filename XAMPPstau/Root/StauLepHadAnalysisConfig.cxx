#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauLepHadAnalysisConfig.h>

namespace XAMPP {
    StauLepHadAnalysisConfig::StauLepHadAnalysisConfig(const std::string& Analysis) : AnalysisConfig(Analysis) {}

    StatusCode StauLepHadAnalysisConfig::initializeCustomCuts() {
        /**
         * @note Skimming cuts used only
         */

        // Cut definitions
        Cut* BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=1", Cut::CutType::CutInt);
        if (!BaselineTauCut->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;

        Cut* SignalTauCut = NewSkimmingCut("N_{#tau}^{signal}==1", Cut::CutType::CutInt);
        if (!SignalTauCut->initialize("n_SignalTau", ">=1")) return StatusCode::FAILURE;

        Cut* SignalTauVeto = NewSkimmingCut("N_{#tau}^{signal}==0", Cut::CutType::CutInt);
        if (!SignalTauVeto->initialize("n_SignalTau", "=0")) return StatusCode::FAILURE;

        Cut* BaselineMuonCut = NewSkimmingCut("N_{#mu}^{baseline} #geq 1", Cut::CutType::CutInt);
        if (!BaselineMuonCut->initialize("n_BaseMuon", ">=1")) return StatusCode::FAILURE;

        Cut* LooseMuonVeto = NewSkimmingCut("N_{#mu}^{loose} = 0", Cut::CutType::CutInt);
        if (!LooseMuonVeto->initialize("n_LooseMuon", "=0")) return StatusCode::FAILURE;

        Cut* BaseMuonVeto = NewSkimmingCut("N_{#mu}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaseMuonVeto->initialize("n_BaseMuon", "=0")) return StatusCode::FAILURE;

        Cut* BaselineElecCut = NewSkimmingCut("N_{e}^{baseline} #geq 1", Cut::CutType::CutInt);
        if (!BaselineElecCut->initialize("n_BaseElec", ">=1")) return StatusCode::FAILURE;

        Cut* BaseElecVeto = NewSkimmingCut("N_{e}^{baseline} = 0", Cut::CutType::CutInt);
        if (!BaseElecVeto->initialize("n_BaseElec", "=0")) return StatusCode::FAILURE;

        Cut* LooseElecVeto = NewSkimmingCut("N_{e}^{loose} = 0", Cut::CutType::CutInt);
        if (!LooseElecVeto->initialize("n_LooseElec", "=0")) return StatusCode::FAILURE;

        Cut* MetCut = NewSkimmingCut("E_{T}^{miss} > 15 GeV", Cut::CutType::CutXAMPPmet);
        if (!MetCut->initialize("MetTST", ">15000.")) return StatusCode::FAILURE;

        Cut* TrigCut = NewSkimmingCut("TrigMatching", Cut::CutType::CutChar);
        if (!TrigCut->initialize("TrigMatching", "=1")) return StatusCode::FAILURE;

        Cut* b_veto = NewSkimmingCut("N_{bjet} == 0", Cut::CutType::CutInt);
        if (!b_veto->initialize("n_BJets", "=0")) return StatusCode::FAILURE;

        // muon event
        CutFlow MuonEvent("MuonEvent");
        MuonEvent.push_back(TrigCut);
        MuonEvent.push_back(BaselineMuonCut);
        MuonEvent.push_back(LooseMuonVeto);
        MuonEvent.push_back(BaseElecVeto);
        MuonEvent.push_back(BaselineTauCut);
        MuonEvent.push_back(MetCut);

        // electron event
        CutFlow ElectronEvent("ElectronEvent");
        ElectronEvent.push_back(TrigCut);
        ElectronEvent.push_back(BaselineElecCut);
        ElectronEvent.push_back(LooseElecVeto);
        ElectronEvent.push_back(BaseMuonVeto);
        ElectronEvent.push_back(BaselineTauCut);
        ElectronEvent.push_back(MetCut);

        // muon-tau event
        CutFlow MuonTau(MuonEvent);
        MuonTau.setName("MuonTau");
        MuonTau.push_back(SignalTauCut);
        ATH_CHECK(AddToCutFlows(MuonTau));

        CutFlow MuonAntiTau(MuonEvent);
        MuonAntiTau.setName("MuonAntiTau");
        MuonAntiTau.push_back(SignalTauVeto);
        MuonAntiTau.push_back(b_veto);
        ATH_CHECK(AddToCutFlows(MuonAntiTau));

        // electron-tau event
        CutFlow ElectronTau(ElectronEvent);
        ElectronTau.setName("ElectronTau");
        ElectronTau.push_back(SignalTauCut);
        ATH_CHECK(AddToCutFlows(ElectronTau));

        CutFlow ElectronAntiTau(ElectronEvent);
        ElectronAntiTau.setName("ElectronAntiTau");
        ElectronAntiTau.push_back(SignalTauVeto);
        ElectronAntiTau.push_back(b_veto);
        ATH_CHECK(AddToCutFlows(ElectronAntiTau));

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
