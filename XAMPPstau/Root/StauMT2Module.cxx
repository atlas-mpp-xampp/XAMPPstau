#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauMT2Module.h>

namespace XAMPP {
    StauMT2Module::~StauMT2Module() {}
    StauMT2Module::StauMT2Module(const std::string& myname) : StauAnalysisModule(myname), m_dec_MT2_min(nullptr), m_dec_MT2_max(nullptr) {}

    StatusCode StauMT2Module::bookVariables() {
        ATH_CHECK(retrieveContainers());
        ATH_CHECK(newVariable("MT2_min", m_dec_MT2_min));
        ATH_CHECK(newVariable("MT2_max", m_dec_MT2_max));
        return StatusCode::SUCCESS;
    }
    StatusCode StauMT2Module::fill() {
        // Arbitraly set the container of interset to be the TauContainer
        ATH_MSG_DEBUG("Start to calculate the MT2");
        xAOD::IParticleContainer::const_iterator begin_itr = m_taus->Container()->begin();
        xAOD::IParticleContainer::const_iterator end_itr = m_taus->Container()->end();
        float MT2_max = FLT_MIN;
        float MT2_min = FLT_MAX;
        for (xAOD::TauJetContainer::const_iterator L = begin_itr; L != end_itr; ++L) {
            const xAOD::IParticle* Lep1 = *L;
            for (xAOD::TauJetContainer::const_iterator L1 = L + 1; L1 != end_itr; ++L1) {
                const xAOD::IParticle* Lep2 = *L1;
                float MT2_tmp = CalculateMT2(Lep1, Lep2, m_met);
                if (MT2_tmp > MT2_max) MT2_max = MT2_tmp;
                if (MT2_tmp < MT2_min) MT2_min = MT2_tmp;
            }
        }
        ATH_CHECK(m_dec_MT2_max->Store(MT2_max));
        ATH_CHECK(m_dec_MT2_min->Store(MT2_min));
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
