#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauMeffModule.h>

namespace XAMPP {
    StauMeffModule::~StauMeffModule() {}
    StauMeffModule::StauMeffModule(const std::string& myname) :
        StauAnalysisModule(myname),
        m_Leptons(nullptr),
        m_dec_HT_jet(nullptr),
        m_dec_HT_lep(nullptr),
        m_dec_HT_tau(nullptr),
        m_dec_Meff(nullptr),
        m_dec_Meff_tau(nullptr),
        m_dec_Pt_Eff(nullptr),
        m_dec_Pt_Eff2(nullptr),
        m_dec_Vec_SumPt(nullptr),
        m_dec_Vec_SumPhi(nullptr),
        m_dec_Met_Centrality(nullptr),
        m_dec_Met_BiSect(nullptr),
        m_dec_SumCosDeltaPhi(nullptr),
        m_dec_CosMinDeltaPhi(nullptr),
        m_dec_PTt(nullptr),
        m_dec_dPhiTauLepMET(nullptr),
        m_dec_CosChi1(nullptr),
        m_dec_CosChi2(nullptr),
        m_dec_mTauTau2(nullptr),
        m_lepton_container(),
        m_save_had_had(false),
        m_save_meff(true),
        m_save_lep_met(true),
        m_save_plane_angles(true) {
        declareProperty("LeptonContainer", m_lepton_container);
        declareProperty("SaveHadHad", m_save_had_had);
    }

    StatusCode StauMeffModule::bookVariables() {
        ATH_CHECK(retrieveContainers());
        m_Leptons = m_XAMPPInfo->GetParticleStorage(m_lepton_container);
        if (m_Leptons == nullptr) {
            ATH_MSG_ERROR("Failed to retrieve the lepton container " << m_lepton_container);
            return StatusCode::FAILURE;
        }
        if (m_save_meff) {
            ATH_CHECK(newVariable("Meff", m_dec_Meff));
            ATH_CHECK(newVariable("Ht_Jet", m_dec_HT_jet));
            ATH_CHECK(newVariable("Ht_Lep", m_dec_HT_lep));
            if (m_save_had_had) {
                ATH_CHECK(newVariable("Ht_Tau", m_dec_HT_tau));
                ATH_CHECK(newVariable("Meff_TauTau", m_dec_Meff_tau));
            }
            ATH_CHECK(newVariable("VecSumPt_LepTau", m_dec_Vec_SumPt));
        }
        if (m_save_lep_met) {
            ATH_CHECK(newVariable("MET_LepTau_VecSumPt", m_dec_Pt_Eff));
            ATH_CHECK(newVariable("MET_LepTau_LeadingJet_VecSumPt", m_dec_Pt_Eff2));
            ATH_CHECK(newVariable("MET_LepTau_VecSumPhi", m_dec_Vec_SumPhi));
            ATH_CHECK(newVariable("MET_Centrality", m_dec_Met_Centrality));
            ATH_CHECK(newVariable("MET_BiSect", m_dec_Met_BiSect));
            ATH_CHECK(newVariable("MET_SumCosDeltaPhi", m_dec_SumCosDeltaPhi));
            ATH_CHECK(newVariable("MET_CosMinDeltaPhi", m_dec_CosMinDeltaPhi));
            ATH_CHECK(newVariable("MET_LepTau_DeltaPhi", m_dec_dPhiTauLepMET));
            ATH_CHECK(newVariable("ColinearMTauTau", m_dec_mTauTau2));
        }

        if (m_save_plane_angles) {
            ATH_CHECK(newVariable("CosChi1", m_dec_CosChi1));
            ATH_CHECK(newVariable("CosChi2", m_dec_CosChi2));
            ATH_CHECK(newVariable("PTt", m_dec_PTt));
        }

        return StatusCode::SUCCESS;
    }
    StatusCode StauMeffModule::fill() {
        // Arbitraly set the container of interset to be the TauContainer
        ATH_MSG_DEBUG("Start to calculate the Meff Ht and Met centrality variables");
        if (m_save_meff) {
            float HtJet = CalculateHt(m_jets->Container());
            float HtTau = CalculateHt(m_taus->Container());
            float HtLep = CalculateHt(m_electrons->Container()) + CalculateHt(m_muons->Container()) + HtTau;
            float Meff = m_met->GetValue()->met() + HtLep + HtJet;

            ATH_CHECK(m_dec_Meff->Store(Meff));
            ATH_CHECK(m_dec_HT_jet->Store(HtJet));
            ATH_CHECK(m_dec_HT_lep->Store(HtLep));
            if (m_save_had_had) {
                ATH_CHECK(m_dec_HT_tau->Store(HtTau));
                float MeffTau = HtTau + m_met->GetValue()->met();
                ATH_CHECK(m_dec_Meff_tau->Store(MeffTau));
            }
        }
        // leptons
        const xAOD::IParticle* lead_lep = nullptr;
        if (m_Leptons->Container()->size() > 0) lead_lep = m_Leptons->Container()->at(0);
        const xAOD::IParticle* sublead_lep = nullptr;
        if (m_Leptons->Container()->size() > 1) sublead_lep = m_Leptons->Container()->at(1);

        // jets
        const xAOD::IParticle* lead_jet = nullptr;
        if (m_jets->Container()->size()) lead_jet = m_jets->Container()->at(0);

        const xAOD::IParticle* sublead_jet = nullptr;
        if (m_jets->Container()->size() > 1) sublead_jet = m_jets->Container()->at(1);
        if (m_save_lep_met) {
            // Centrality
            ATH_CHECK(m_dec_Met_Centrality->Store(METcentrality(lead_lep, sublead_lep, m_met)));
            // BiSect
            ATH_CHECK(m_dec_Met_BiSect->Store(METbisect(lead_lep, sublead_lep, m_met)));
            // pt ( l1 + l2)
            ATH_CHECK(m_dec_Vec_SumPt->Store(VecSumPt(lead_lep, sublead_lep)));
            // pt (l1 + l2 + met)
            TLorentzVector v = (lead_lep ? lead_lep->p4() : TLorentzVector()) + (sublead_lep ? sublead_lep->p4() : TLorentzVector());
            TLorentzVector met;
            met.SetPtEtaPhiM(m_met->GetValue()->met(), 0, m_met->GetValue()->phi(), 0);
            v += met;
            ATH_CHECK(m_dec_Pt_Eff->Store(v.Pt()));
            ATH_CHECK(m_dec_Vec_SumPhi->Store(v.Phi()));
            // pt (l1 + l2 + met + lead jet)
            TLorentzVector v2 = v;
            if (lead_lep) { v += lead_lep->p4(); }
            ATH_CHECK(m_dec_Pt_Eff2->Store(v2.Pt()));
            // Cos sum delta phi
            ATH_CHECK(m_dec_SumCosDeltaPhi->Store(SumCosDeltaPhi(lead_lep, sublead_lep, m_met->GetValue())));
            // Cos min delta phi
            ATH_CHECK(m_dec_CosMinDeltaPhi->Store(CosMinDeltaPhi(lead_lep, sublead_lep, m_met->GetValue())));
            // delta phi between MET and the lep-tau system
            ATH_CHECK(m_dec_dPhiTauLepMET->Store(PairMetDeltaPhi(lead_lep, sublead_lep, m_met->GetValue())));

            float mtautau2 = -FLT_MAX;
            if (lead_lep && sublead_lep) {
                float det = lead_lep->p4().Px() * sublead_lep->p4().Py() - lead_lep->p4().Py() * sublead_lep->p4().Px();
                if (std::fabs(det) > 1.e-6) {
                    float x1 = (met.Px() * sublead_lep->p4().Py() - sublead_lep->p4().Px() * met.Py()) / det;
                    float x2 = (met.Py() * lead_lep->p4().Px() - met.Px() * lead_lep->p4().Py()) / det;
                    mtautau2 = 2 * lead_lep->p4().Dot(sublead_lep->p4()) * (1 + x1) * (1 + x2);
                    mtautau2 = (mtautau2 > 0 ? 1. : -1.) * std::sqrt(std::fabs(mtautau2));
                }
            }
            ATH_CHECK(m_dec_mTauTau2->Store(mtautau2));
        }
        if (m_save_plane_angles) {
            // cos chi, angle between the pair system and the leading jet in the event
            ATH_CHECK(m_dec_CosChi1->Store(CosChi1(lead_lep, sublead_lep, lead_jet)));
            ATH_CHECK(m_dec_CosChi2->Store(CosChi2(lead_lep, sublead_lep, lead_jet, sublead_jet)));
            // PTt variable inspired by the di-photon search analysis
            ATH_CHECK(m_dec_PTt->Store(PTt(lead_lep, sublead_lep)));
        }

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
