#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauMCTModule.h>

namespace XAMPP {
    StauMCTModule::~StauMCTModule() {}
    StauMCTModule::StauMCTModule(const std::string& myname) : StauAnalysisModule(myname), m_dec_MCT(nullptr) {}

    StatusCode StauMCTModule::bookVariables() {
        ATH_CHECK(retrieveContainers());
        ATH_CHECK(newVariable("MCT", m_dec_MCT));
        return StatusCode::SUCCESS;
    }
    StatusCode StauMCTModule::fill() {
        // Arbitraly set the container of interset to be the TauContainer
        ATH_MSG_DEBUG("Start to calculate the MCT");

        // taus
        const xAOD::IParticle* lead_tau = nullptr;
        if (m_taus->Container()->size() > 0) lead_tau = m_taus->Container()->at(0);
        const xAOD::IParticle* sublead_tau = nullptr;
        if (m_taus->Container()->size() > 1) sublead_tau = m_taus->Container()->at(1);

        ATH_CHECK(m_dec_MCT->Store(calc_mct(lead_tau, sublead_tau)));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
