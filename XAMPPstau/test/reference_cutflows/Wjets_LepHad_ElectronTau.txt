Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto             DSID_364159_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  84614 ± 290.88
PassGRL                  84614 ± 290.88
passLArTile              84614 ± 290.88
Trigger                  68734 ± 262.17
HasVtx                   68734 ± 262.17
BadJet                   68734 ± 262.17
CosmicMuon               68686 ± 262.08
BadMuon                  68589 ± 261.90
TrigMatching             61582 ± 248.16
N_{e}^{baseline} #geq 1  886   ± 29.77
N_{e}^{loose} = 0        51    ± 7.14
N_{#mu}^{baseline} = 0   10    ± 3.16
N_{#tau}^{baseline}>=1   1     ± 1.00
E_{T}^{miss} > 15 GeV    1     ± 1.00
N_{#tau}^{signal}==1     1     ± 1.00
Final                    1     ± 1.00
############################################
Printing raw events
CutFlowHisto             DSID_364160_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  28886 ± 169.96
PassGRL                  28886 ± 169.96
passLArTile              28886 ± 169.96
Trigger                  23906 ± 154.62
HasVtx                   23906 ± 154.62
BadJet                   23906 ± 154.62
CosmicMuon               23864 ± 154.48
BadMuon                  23812 ± 154.31
TrigMatching             21349 ± 146.11
N_{e}^{baseline} #geq 1  469   ± 21.66
N_{e}^{loose} = 0        37    ± 6.08
N_{#mu}^{baseline} = 0   7     ± 2.65
############################################
Printing raw events
CutFlowHisto             DSID_364161_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  39163 ± 197.90
PassGRL                  39163 ± 197.90
passLArTile              39163 ± 197.90
Trigger                  31587 ± 177.73
HasVtx                   31587 ± 177.73
BadJet                   31587 ± 177.73
CosmicMuon               31292 ± 176.90
BadMuon                  31201 ± 176.64
TrigMatching             27614 ± 166.17
N_{e}^{baseline} #geq 1  998   ± 31.59
N_{e}^{loose} = 0        65    ± 8.06
N_{#mu}^{baseline} = 0   8     ± 2.83
N_{#tau}^{baseline}>=1   1     ± 1.00
E_{T}^{miss} > 15 GeV    1     ± 1.00
############################################
