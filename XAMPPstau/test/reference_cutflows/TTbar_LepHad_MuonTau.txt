Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto               DSID_410470_CutFlow
Systematic variation       Nominal   
##############################################
Cut                        Yields    
##############################################
Initial                    114802 ± 338.82
PassGRL                    114802 ± 338.82
passLArTile                114802 ± 338.82
Trigger                    96460  ± 310.58
HasVtx                     96460  ± 310.58
BadJet                     96460  ± 310.58
CosmicMuon                 95128  ± 308.43
BadMuon                    94915  ± 308.08
TrigMatching               70423  ± 265.37
N_{#mu}^{baseline} #geq 1  22984  ± 151.60
N_{#mu}^{loose} = 0        19551  ± 139.82
N_{e}^{baseline} = 0       12320  ± 111.00
N_{#tau}^{baseline}>=1     3208   ± 56.64
E_{T}^{miss} > 15 GeV      3063   ± 55.34
N_{#tau}^{signal}==1       1128   ± 33.59
Final                      1128   ± 33.59
##############################################
