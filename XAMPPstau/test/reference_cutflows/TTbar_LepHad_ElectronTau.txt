Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto             DSID_410470_CutFlow
Systematic variation     Nominal   
############################################
Cut                      Yields    
############################################
Initial                  114802 ± 338.82
PassGRL                  114802 ± 338.82
passLArTile              114802 ± 338.82
Trigger                  96460  ± 310.58
HasVtx                   96460  ± 310.58
BadJet                   96460  ± 310.58
CosmicMuon               95128  ± 308.43
BadMuon                  94915  ± 308.08
TrigMatching             70423  ± 265.37
N_{e}^{baseline} #geq 1  51086  ± 226.02
N_{e}^{loose} = 0        44991  ± 212.11
N_{#mu}^{baseline} = 0   37418  ± 193.44
N_{#tau}^{baseline}>=1   2921   ± 54.05
E_{T}^{miss} > 15 GeV    2780   ± 52.73
N_{#tau}^{signal}==1     968    ± 31.11
Final                    968    ± 31.11
############################################
