# Git and Installation Instructions


## XAMPPBase

Prepare the structure for cmake

```
mdkir XAMPP_R21
cd XAMPP_R21
mkdir build/ run/
git clone ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git --recursive source -b <mybranch>
```

The `--recursive` command argument is important in order to checkout the submodule dependencies as well. 
If you want to update your local code to the current master version:

```
cd XAMPPstau
git pull origin master
```

If you want to check-in you local changes to your branch, use:

```
git commit -a -m "<your submit message>"
git push
```
## Compile

Go to the `build/` directory and setup the analysis environment as follows

```
setupATLAS
asetup AthAnalysis,21.2.152,here
```
At later logins, you can restore your previous Athena session by typing in `build/`
```
asetup --restore
```
To check which release series you should be using, please look at (Ath)AnalysisRelease [twiki page](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease). 
The available 21.2.X Athena analysis releases can be seen also on cvmfs

```
/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AthAnalysis/
```

and execute `cmake` as follows

```
cmake ../source/
```

Find out the number of CPUs of your working computing node as follows

```
nproc
```

and use that number to compile (for example #CPUs=64)

```
make -j64
```

## XAMPPstau

Repeat the steps for the XAMPPstau package as written above for the XAMPPBase. Checkout the package in your branch
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPstau.git -b myName
```
Please, note the `--recursive` argument. Without this, the submodules of the projects will not be cloned. 

If you have already cloned the project without specifying the `--recursive` argument, you can do the following
```
git pull && git submodule init && git submodule update && git submodule status
```
For newest git versions you can use
```
git submodule update --recursive
```
and if that's the first time you checkout the repo you need to use `--init` first:
```
git submodule update --init --recursive
```

The git submodule update command actually tells Git that you want your submodules to each check out the commit already specified in the index of the superproject,
for example the "EventShapeVariables". If you want to update your submodules to the latest commit available from their remote, 
you will need to do this directly in the submodules. In summary:
```
# get the submodule initially
git submodule add <submodule URL address>
git submodule init

# time passes, submodule upstream is updated
# and you now want to update

# change to the submodule directory
cd submodule_dir

# checkout desired branch
git checkout master

# update
git pull

# get back to your project root
cd ..

# now the submodules are in the state you want, so
git commit -am "Pulled down update to submodule_dir"
```

Otherwise, you can use the one-liner command:
```
git submodule foreach git pull origin master
```
To summarize, in order to checkout proper commits in your submodules you should update them after pulling using the above command. If the above command
seems not to behave well, you may want to use:
```
git submodule update --init
```

To add a deleted or missing submodule:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/restframes.git RestFrames
git submodule update --init --recursive
```

After doing all this, you move to the package's directory
```
cd XAMPPstau/
```
to get the latest updates
```
git pull origin master
```
Get to the `build/` directory and run again
```
cmake ../source/
```
You should see the new package being indeed recognized by cmake

> Configuring the build of package: XAMPPstau

Compile again
```
make -j64
```

# Running the  XAMPPstau - Local Test

Once you compile successfully, you need to inform your environment about the new executables. So, still in the `build/` directory try to do

```
source x86_64-slc6-gcc62-opt/setup.sh
```

To run a single test, create frist a temporary directory to keep the output in

```
mkdir /ptmp/mpp/${USER}/TestStau/
```
and then execute the Athena job options (either runLepHad.py or runHadHad.py)  as in the following single-input-file example:
```
python $TestArea/XAMPPbase/XAMPPbase/python/runAthena.py \
       -o /ptmp/mpp/${USER}/TestStau/TestStau.root \
       --filesInput /ptmp/mpp/${USER}/DirectStau/SUSY18/DAOD_SUSY18.MC16.pool.root \
       --jobOptions XAMPPstau/share/runLepHad.py \
       --evtMax 10000 \
       --noSyst
```
Keep in mind that `XAMPPbase` is a (git) submodule of the `XAMPPstau` package. The full command arguments can be found in `XAMPPbase/python/runSUSYAnalysis.py` that are inherited to `XAMPPbase/python/runAthena.py`. If you want to direct the output in your run directory, then use
```
python $TestArea/XAMPPbase/XAMPPbase/python/runAthena.py \
       --jobOptions XAMPPstau/share/runLepHad.py \
       --inFile <DAOD file> 
       --noSyst | tee log.txt
```


# Running the  XAMPPstau - Run on the Grid

After setting `asetup`, you need to setup rucio and panda as well:
```
localSetupRucioClients
voms-proxy-init -voms atlas
localSetupPandaClient
```
Alternatively, you can do
```
setupATLAS
lsetup "asetup AnalysisBase,21.2.5" panda   # order here does not matter
```
and it set up all for you in the correct order.

The following commands will then launch a production on the grid
```
python $TestArea/XAMPPstau/python/SubmitToGrid.py \
       --sampleList $TestArea/XAMPPstau/data/SampleLists/data15_13TeV/DataGRL15.txt \
       --stream HadHad
       -noSyst \
       --production Testv02a
```

# Creating sample lists

Once your ntuple production on the grid is complete, the next step is to organize the samples in configuration files. 
Supposing that you have set `-destSE=MPPMU_LOCALGROUPDISK` in the pathena setings, the way to create a list of the samples with physical names
on a destination storage element is described below.

First of all, you need to setup a grid environment
```
localSetupRucioClients
voms-proxy-init -voms atlas
localSetupPyAMI
```
To gather the background samples, execute the following command in the `run/` directory
```
python $TestArea/XAMPPbase/python/ListGroupDisk.py \
       -p user.zenon p3212 Testv03a \
       -e StauStau \
       -o StauBkg
```
The option `-e` will exclude all samples that contain any of the keys in the list you provided. 
The directory `StauBkg` will contain a text file with all background samples which satify the keywords set after the command argument `-p`. 

Likewise for the signal samples,
```
python $TestArea/XAMPPbase/python/ListGroupDisk.py \
       -p user.zenon p3212 Testv03a StauStau\
       -o StauSig
```
The commands to generate the configuration files required by the plotting framework are given below. For the background samples,
```
python $TestArea/XAMPPplotting/scripts/Stau/CreateDataConfigs.py \
       -i StauBkg/MPPMU_LOCALGROUPDISK_2017-08-25_user.zenon_p3212_Testv03a_exl_StauStau.txt \
       -o $TestArea/XAMPPplotting/data/InputConf/Stau/Background/ \
       -p mc16_13TeV
```
and for the signal samples,
```
python $TestArea/XAMPPplotting/scripts/Stau/CreateDataConfigs.py \
       -i StauSig/MPPMU_LOCALGROUPDISK_2017-08-25_user.zenon_p3212_StauStau_Testv03a.txt \
       -o $TestArea/XAMPPplotting/data/InputConf/Stau/Signal/ \
       -p mc16_13TeV
```

# Plotting the results

Repeat the installation procedure for the [XAMPPplotting](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting) project.

Create an input configuration file for your test ntuple:
```
 echo "Input <your output ntuple file>" > StauTest.conf
```
and create an ntuple as follows
```
WriteDefaultHistos -r $TestArea/XAMPPplotting/data/RunConf/Stau/SR/Mu_0jet.conf \
                   -h $TestArea/XAMPPplotting/data/HistoConf/Stau/Histos.conf \
                   -i StauTest.conf \
                   -o StauTest.root
```
You can also use an existing configuration file of a finished production and test it on a limited number of events, 
```
WriteDefaultHistos -r $TestArea/XAMPPplotting/data/RunConf/Stau/SR/Mu_0jet.conf \
                   -h $TestArea/XAMPPplotting/data/HistoConf/Stau/Histos.conf \
                   -i XAMPPplotting/data/InputConf/Stau/Signal/mc16_13TeV.393635.MGPy8EG_A14N23LO_StauStau_direct_200p0_1p0_2L8.Testv03a.r9364_r9315_p3212.conf \
                   -o StauTest.root \
                   -n 1000
```

To produce ntuples with plots on a set of samples (background, signal, data) and a combination of regions (CR, SR) we choose of launching an array of jobs on a 
batch platform as follows
```
python $TestArea/XAMPPplotting/python/SubmitToBatch.py \
    -J StauBkgSR \
    -I XAMPPplotting/data/InputConf/Stau/Background \
    -R XAMPPplotting/data/RunConf/Stau/SR/ \
    -H XAMPPplotting/data/HistoConf/Stau/Histos.conf \
    --RunTime 01:59:00
```
The above example will produce all possible histograms defined in the `Histos.conf` configuration file, in all SRs contained in `/RunConf/Stau/SR/` and for all background samples listed in `InputConf/Stau/Background`. 

# Creating final plots

Polished MC-Data plots can be generated as follows
```
python $TestArea/XAMPPplotting/python/DataMCPlots.py \
    -c $TestArea/XAMPPplotting/python/DSConfigs/Stau.py \
    -o StauHistos \
    --noSignal \
    --noSyst \
    --LowerPadRanges 0.75 1.25 \
    --doLogY \
    --OutFileType eps
```
The `DSConfigs/Stau.py` file itself is configurable.

# Git Tags

Annotated tags
--------------
Creating an annotated tag in Git is simple. The easiest way is to specify `-a` when you run the tag command. 
An example how to tag is given below. First of all switch to your favorite branch
```
    git checkout <my branch>
```
and then
```
    git tag -a LepHad.V03 -m 'Moriond 2018 pre-final ntuple production'
```
within a specific forked branch or the master branch. The `-m` specifies a tagging message, which is stored with the tag. You can see the tag data along with the commit that was tagged by using the `git show` command:
```
    git show LepHad.V03
```  
That shows the tagger information, the date the commit was tagged, and the annotation message before showing the commit information. To list the tags do:
```
    git tag
```    
This kind of tags should be pushed to master:
```
git push  origin LepHad.V03b
```

Checkout a production tag
-------------------------
You can checkout a tag used in a production as follows:
```
git checkout tags/LepHad.v21 -b LepHad.v21_myname
```

# Troubleshooting

## File GUID Problems

>AthenaPoolConverter             ERROR Failed to convert persistent object to transient: FID "30B1191D-342F-F34E-B887-1EF7384D2CAD" is not existing in the catalog ( POOL : "PersistencySvc::UserDatabase::connectForRead" from "PersistencySvc" )
 
_Hint_: the `PoolFileCatalog.xml` catalogue is either missing from your running directory or it is outdated.

__Solution__: either to delete your `PoolFileCatalog.xml` file or update it using the command
```
 pool_insertFileToCatalog.py <pool file>
```

## CMake Errors
>CMake Error at /cvmfs/atlas.cern.ch/repo/sw/software/21.2/AthAnalysis/21.2.2/InstallArea/x86_64-slc6-gcc62-opt/cmake/modules/AtlasLibraryFunctions.cmake:269 (target_link_libraries):
>  Attempt to add link library
>  "$<INSTALL_INTERFACE:$<INSTALL_INTERFACE:${LCG_RELEASE_BASE}/LCG_88/Boost/1.62.0/x86_64-slc6-gcc62-opt/lib/libboost_atomic-gcc62-mt-1_62.so>>"
>  to target "XAMPPplottingLib" which is not built in this directory.
>Call Stack (most recent call first):
>  c1531ada-5d11-4b64-bd4b-c206bf8d5cc0/_CPack_Packages/Linux/TGZ/sources.98791ff4-65d9-4752-bcc4-a7052770e1bc/usr/WorkDir/21.2.0/InstallArea/x86_64-slc6-gcc62-opt/src/XAMPPplotting/CMakeLists.txt:28 (atlas_add_library)

_Hint_: unnecessary files in the `source/` directory are spoilers.

__Solution__: in a virgin session, go to the `source/` directory, `asetup` your favourite analysis release, clean up all uncessary visible and hidden files or directories, get to the `build/` directory, clean everything with `rm -fr ./*`, execute `cmake ../source`, compile with `make` and finally setup the platform setup.sh script. 

# FAQ

- Where are the default GRL and PRW files being configured?

Look in the `ParseBasicConfigsToHelper` method of `XAMPPbase/share/BaseToolSetup.py` and for the components `BaseHelper.PRWLumiCalcFiles` and `BaseHelper.PRWConfigFiles`.

- How do I delete my transferred data on the MPP cluster?

In your `run/` directory, create a list of your transfers:
```
localSetupRucioClients
voms-proxy-init -voms atlas

python $TestArea/XAMPPbase/python/ListGroupDisk.py --MyRequests
```
and then use the output file to delete them:
```
python $TestArea/XAMPPbase/python/DeleteFromGroupDisk.py -l MyRequestTo_<which disk>_<when>.txt
```

- My push request fails the CI because of a git submodule

You are likely using the package itself and not its submodule. Get it in the directory of this package and do
```
git push
```
Do not merge the request. This will synchronize the git hash of the local package to the git submodule.


- I get `symbol lookup error` rutime problems when I run the plotting framework 
```
WriteDefaultHistos: symbol lookup error: /cvmfs/atlas.cern.ch/repo/sw/software/21.2/sw/lcg/releases/ROOT/6.08.06-c8fb4/x86_64-slc6-gcc62-opt/lib/libNetxNG.so: undefined symbol: _ZN5XrdCl3URLC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
WriteDefaultHistos        INFO    Environment initialised for data access
AnalysisSetup::CreateNewRegion()    -- INFO: Create a new analysis region
```
Please do not setup `rucio` in the same session as for the analysis release.

### Git unmerged problems

For this problem
```
# Unmerged paths:
#   (use "git add/rm <file>..." as appropriate to mark resolution)
#
#       both modified:      XAMPPbase
 ```
you simply need to do:
```
git add XAMPPbase
``
